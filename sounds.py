import numpy as np
import math
import pyaudio
import threading
import thread
import datetime
from time import sleep
# sudo apt-get install python-pyaudio

PyAudio = pyaudio.PyAudio


class SoundManager(threading.Thread):
    """
    Used to play and create sine wave sounds.
    """
    double = False

    def __init__(self):
        super(SoundManager, self).__init__()
        self.bitrate = 16000
        self.p = PyAudio()
        self.stream = self.p.open(
            format=pyaudio.paFloat32,
            channels=1,
            rate=self.bitrate,
            output=True)
        self.alive = False
        self.wave = None

    def create_wave(self, on, off, frequency, fade):
        frames = int(self.bitrate * on)
        wave = []

        for x in xrange(frames):
            wave.append(
                min([1, float(x) / (self.bitrate * fade)]) *
                min([1, float(frames - x) / (self.bitrate * fade)]) *
                math.sin((x) / ((self.bitrate / frequency) / math.pi)))
        wave = np.array(wave)
        return wave.astype(np.float32).tostring()

    def play_sound(self, duration, frequency):
        if not self.alive:
            wave = self.create_wave(duration, 0, frequency, 0.01)
            self.stream.write(wave)

    def set_mode(self, on, off, frequency, delay=None, _sleep=False, win=False):
        if not win:
            if delay is not None and not _sleep:
                thread.start_new_thread(
                    self.set_mode,
                    (on, off, frequency, delay, True))
                return
            elif _sleep:
                sleep(delay)
                if self.double:
                    return
            self.double = False
        self.wave = self.create_wave(on, off, frequency, fade=0.01)
        self.off = off
        self.on = on
        self.last_timer = datetime.datetime.now()

    def set_double(self, low, high, sleep_amount=None):
        if sleep_amount is not None:
            sleep(sleep_amount)
        duration = datetime.datetime.now() - self.last_timer
        secs = (duration.microseconds + duration.seconds * 1000000) / 1000000.0
        if secs < self.on + self.off:
            thread.start_new_thread(
                self.set_double, (low, high, secs))
        self.wave = self.create_wave(0.2, 0.5, low, fade=0.01)
        self.wave2 = self.create_wave(0.2, 0.5, high, fade=0.01)
        self.on, self.off = 0.2, 0.5
        self.double = True

    def run(self):
        self.alive = True
        while self.alive:
            while not self.double and self.wave is not None and self.alive:
                t = 0
                self.last_sound = datetime.datetime.now()
                self.stream.write(self.wave)
                while t < self.on + self.off:
                    sleep(0.001)
                    t = datetime.datetime.now() - self.last_sound
                    t = (t.microseconds + t.seconds * 1000000) / 1000000.0
            self.stream.write(self.wave)
            wave_type = True
            while self.double and self.wave is not None and self.alive:
                t = 0
                self.last_sound = datetime.datetime.now()
                if wave_type:
                    self.stream.write(self.wave)
                else:
                    self.stream.write(self.wave2)
                wave_type = not wave_type
                while t < self.on + self.off:
                    sleep(0.001)
                    t = datetime.datetime.now() - self.last_sound
                    t = (t.microseconds + t.seconds * 1000000) / 1000000.0
        self.kill()

    def stop(self):
        '''
        Finishes playing the current sound and then stops
        '''
        self.alive = False

    def kill(self):
        self.alive = False
        self.stream.close()
        self.stream.stop_stream()
        self.p.terminate()
