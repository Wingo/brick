import json
from matplotlib import pyplot as plt
f = open('results/outusers.json', 'r')
data = json.load(f)
f.close()

print data
plt.figure()
for u in [k for k in data.keys() if k.lower() != 'osku']:
    print data[u]
    points = [es for es in data[u] if 'False' in es[-1]]
    if len(points) == len(list(set([p[-1] for p in points]))):
        plt.plot([es[3] for es in points], [es[0] for es in points])
plt.savefig('useresults.pdf')

plt.figure()
stats = []
for u in [k for k in data.keys() if k.lower() != 'osku']:
    print data[u]
    points = [es for es in data[u] if 'False' in es[-1]]
    points.sort(key=lambda x: x[3])
    if len(points) == 2 and len(points) == len(list(set([p[-1] for p in points]))):
        stats.append(points[1][0] / float(points[0][0]))
plt.hist(stats)
plt.xlim([0, max(stats)])
plt.title('Delayed vs. real time performance')
plt.xlabel('Times more difficult')
plt.ylabel('Number of people')
plt.savefig('userstats.pdf')
