from Tkinter import *
import cv2
from PIL import Image, ImageTk


def prompt(message):
    def kill(top):
        top.quit()
        top.destroy()
    top = Tk()
    lbl = Label(text=message)

    lbl.grid(row=0, column=0)
    b = Button(text='OK', command=lambda: kill(top))
    b.grid(row=1, column=0)
    top.mainloop()


def select_camera(imglist):
    global selected
    selected = -1
    max_dimensions = (400, 400)
    top = Tk()

    def assign(ind, top):
        global selected
        selected = ind
        print 'assigned to:', ind
        top.quit()
        top.destroy()

    btns = []
    for ind, img in enumerate(imglist):
        dims = img.shape
        scale = min([
            float(max_dimensions[0]) / dims[0],
            float(max_dimensions[1]) / dims[1]])
        resized_image = cv2.resize(img, None, fx=scale, fy=scale)
        itk = ImageTk.PhotoImage(Image.fromarray(resized_image))
        lbl = Label(image=itk)
        lbl.image = itk
        lbl.grid(row=0, column=ind)
        btns.append(Button(
            text='Camera %s' % (ind + 1),
            command=lambda camera_index=ind: assign(camera_index, top)))
        btns[-1].grid(row=1, column=ind)
    top.mainloop()
    return selected
