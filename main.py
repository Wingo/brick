from utils import take_picture, resize_image, apply_white_balance
from gui import GUIrunner
from cv_logic import initialize
import cv2
import sys
from stopwatch import start, lap_and_stop, get_time
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt

start('Total')
training_scheme = [2, 5]
test_scheme = [5, 7]


settings, objects, camera_choice, wb, camera, home, control_image = initialize('--train' in sys.argv)

p = take_picture(camera_choice)
p = apply_white_balance(p, wb)
p = resize_image(p)
gr = GUIrunner(
    cv2.cvtColor(p, cv2.COLOR_BGR2RGB), settings, objects, camera_choice,
    wb, [training_scheme, test_scheme], camera, home, control_image)
gr.main()
print 'done'
lap_and_stop('Total')
