import json
import datetime
import random
import cv2
from Tkinter import *
import numpy as np
from PIL import Image, ImageTk
from cv_logic import Exercise
from stopwatch import time_it
from utils import stop_recording
from oskui import get_files
from matplotlib import pyplot as plt
from params import VIDEO_RESOLUTION
import analyze_data

class GUIrunner(object):
    """

    """
    e = None

    def __init__(
            self, video,
            settings, objects,
            camera_choice, wb,
            schemes, camera, home, control_image):
        super(GUIrunner, self).__init__()
        self.video = video
        self.settings = settings
        self.objects = objects
        self.camera_choice = camera_choice
        self.camera = camera
        self.wb = wb
        self.schemes = schemes
        self.home = home
        self.control_image = control_image

    def _show_data(self):
        delay = 0 if self.settings['delay'] is None else self.settings['delay']
        img = show_data(delay)
        self.update_video(img)

    def enable(self, mode):
        if mode:
            self.toggle.configure(text='Start')
            self.toggle.configure(command=self.start_exercise)
            state = NORMAL
            self.e = None
        else:
            self.toggle.configure(text='Stop')
            self.toggle.configure(command=self.stop_exercise)
            state = DISABLED
        self.fb_b.configure(state=state)
        self.mode_b.configure(state=state)
        self.save.configure(state=state)
        self.name_entry.configure(state=state)
        self.delay_minus.configure(state=state)
        self.delay_plus.configure(state=state)

    def fb_button(self, mode=None):
        '''
        @params:
        - mode: integer, feedback mode
            0: verbal feedback, 1: manual feedback, 2: autofeedback
        '''
        if mode is not None:
            self.settings['feedback_mode'] = mode
        else:
            self.settings['feedback_mode'] = (
                self.settings['feedback_mode'] + 1) % 3
        if self.settings['feedback_mode'] == 2:
            self.settings['auto_tick'] = True
        else:
            self.settings['auto_tick'] = False
        if self.settings['feedback_mode'] == 0:
            self.fb_l.configure(text='Verbal')
        elif self.settings['feedback_mode'] == 1:
            self.fb_l.configure(text='Manual')
        else:
            self.fb_l.configure(text='Automatic')

    def mode_button(self, mode=None):
        if mode is not None:
            self.settings['training'] = mode
        else:
            self.settings['training'] = not self.settings['training']
        if self.settings['training']:
            self.scheme = self.schemes[0]
            self.mode_l.configure(text='Practice mode')
        else:
            self.scheme = self.schemes[1]
            self.mode_l.configure(text='Experiment mode')

    def mouse_pos(self, event):
        if self.e is not None and self.e.alive and self.settings['feedback_mode'] == 1:
            self.e.dst_to_wave(event.y_root)

    def increase_delay(self):
        if self.settings['delay'] is None:
            self.settings['delay'] = 0
        self.settings['delay'] += 1
        if self.settings['delay'] > 6:
            self.settings['delay'] = 6
        self.delay_l.configure(text='Delay: %s' % self.settings['delay'])

    def decrease_delay(self):
        if self.settings['delay'] is not None:
            self.settings['delay'] -= 1
            if self.settings['delay'] < 1:
                self.settings['delay'] = None
        self.delay_l.configure(text='Delay: %s' % self.settings['delay'])

    @time_it
    def update_video(self, frame):
        start = datetime.datetime.now()
        self.video = frame
        ar = Image.fromarray(self.video)
        itk = ImageTk.PhotoImage(ar)
        diff = (datetime.datetime.now() - start).microseconds
        if diff < 50000:
            self.lbl.configure(image=itk)
            self.lbl.image = itk

    @time_it
    def start_exercise(self):
        if len(self.settings['username']) > 3:
            if not self.settings['training']:
                random.shuffle(self.scheme)
            self.e = Exercise(
                self.camera_choice,
                self.camera,
                self.settings,
                self.objects,
                self.scheme,
                self.wb,
                self.home,
                self,
                self.control_image)
            self.e.start()
            self.enable(False)

    def stop_exercise(self):
        self.e.alive = False

    def save_username(self):
        if self.name_entry.get() and len(self.name_entry.get()) > 3:
            self.settings['username'] = self.name_entry.get()
            self.toggle_start()

    def set_object(self, *args):
        for o in self.objects:
            if o['name'] == self.object_choice.get():
                o['active'] = True
                self.settings['active_object'] = o['name']
            else:
                o['active'] = False
        self.toggle_start()

    def toggle_start(self, *args):
        if self.name_entry.get() and len(self.name_entry.get()) > 3:
            if 'Please' not in self.object_choice.get():
                self.toggle.configure(state=NORMAL)

    def main(self):
        def kill(top):
            top.quit()
            top.destroy()

        top = Tk()

        itk = ImageTk.PhotoImage(Image.fromarray(self.video))
        self.lbl = Label(image=itk)
        self.lbl.image = itk
        self.lbl.grid(row=0, column=0, rowspan=8, columnspan=2)

        self.toggle = Button(
            text='Start', command=self.start_exercise, state=DISABLED)
        self.toggle.grid(row=9, column=0)

        fb = Label()
        fb.grid(row=0, column=2)

        self.fb_l = Label()
        self.fb_l.grid(row=1, column=2, sticky=W)

        self.fb_b = Button(text='Switch', command=self.fb_button)
        self.fb_b.grid(row=1, column=2, sticky=E)
        self.fb_button(2 if self.settings['auto_tick'] else 1)

        self.mode_l = Label()
        self.mode_l.grid(row=2, column=2, columnspan=2, sticky=W)

        self.mode_b = Button(text='Switch', command=self.mode_button)
        self.mode_b.grid(row=2, column=2, sticky=E)
        self.mode_button(self.settings['training'])

        self.name_entry = Entry()
        self.name_entry.grid(row=3, column=2, sticky=S)

        self.save = Button(text='Save', command=self.save_username)
        self.save.grid(row=4, column=2, sticky=N)

        self.delay_l = Label(text='Delay: %s' % self.settings.get('delay', None))
        self.delay_l.grid(row=5, column=2, columnspan=2)

        self.delay_minus = Button(text='-', command=self.decrease_delay)
        self.delay_minus.grid(row=6, column=2, sticky=E)

        self.delay_plus = Button(text='+', command=self.increase_delay)
        self.delay_plus.grid(row=6, column=2, sticky=W)

        self.object_choice = StringVar(top)
        self.object_choice.set('Please choose!')
        self.object_choice.trace("w", self.set_object)
        for o in self.objects:
            if o['active']:
                self.object_choice.set(o['name'])
                break

        options = [o['name'] for o in self.objects]
        options = options if len(options) else ['None']
        w = OptionMenu(top, self.object_choice, *options)
        w.grid(row=7, column=2)

        show_data = Button(text='Show data', command=self._show_data)
        show_data.grid(row=8, column=2)

        top.bind('<Motion>', self.mouse_pos)
        top.mainloop()
        stop_recording(self.camera)



def show_data(ind=0):
    fs = get_files('data')
    numbers = [int(f.split('_')[-1].split('.')[0]) if 'tracker' in f else int(f.split('_')[-2]) for f in fs if '.dat' in f]
    numbers.sort()
    n = numbers[-(ind + 1)]
    file = [f for f in fs if '.dat' in f and str(n) in f][0]
    f = open('data/' + file, 'r')
    data = json.load(f)
    f.close()

    score, mean_du, mean_di, scores, rel_distances, rel_durations = analyze_data.score_dataset(data)
    scoredata = analyze_data.analyze_data()
    mode = analyze_data.settings_to_mode(data['settings'])
    position = analyze_data.get_position(score, mode, scoredata)

    plt.figure()
    plt.subplot(1, 2, 1)
    for i, e in enumerate(data['exercises']):
        plt.plot([l[1] for l in e['locations']], [l[0] for l in e['locations']], c=str(i / float(len(data['exercises']))))
        plt.xlim([0, VIDEO_RESOLUTION[1]])
        plt.ylim([0, VIDEO_RESOLUTION[0]])
        plt.scatter(e['target'][1], e['target'][0], marker='o', c=str(i / float(len(data['exercises']))))
    plt.scatter(e['locations'][0][1], e['locations'][0][0], color='red', marker='x')
    plt.xlabel('X coordinate')
    plt.ylabel('Y coordinate')
    plt.gca().invert_xaxis()
    plt.gca().invert_yaxis()


    plt.subplot(1, 2, 2)
    for i, e in enumerate(data['exercises']):
        plt.plot(
            (np.array(e['times']) - e['times'][0]) / 1000000.0,
            [np.sqrt(np.abs(l[0] - e['target'][0]) ** 2 + np.abs(l[1] - e['target'][1]) ** 2) for l in e['locations']],
            c=str(i / float(len(data['exercises']))))
    plt.xlabel('Time [seconds]')
    plt.ylabel('Distance [px]')
    plt.suptitle("%s's score: %s, position: %s" % (data['settings']['username'], score, position))

    plt.subplots_adjust(left=0.125, bottom=0.1, right=0.9, top=0.9, wspace=0.4, hspace=0.2)

    plt.savefig('results.jpg')
    img = cv2.imread('results.jpg')


    return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
