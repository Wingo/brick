import json
import csv 

# load json file
with open('outdata.json') as json_file:    
    outdata = json.load(json_file)

# init csv file
csv_file = open('outdata.csv', 'w')
writer = csv.writer(csv_file, delimiter=',')

# write data in csv file and close
writer.writerows(outdata)
csv_file.close()
