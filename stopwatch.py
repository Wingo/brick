'''
    Stopwatch.py is used to track duration of different operation.
    Before the operation call start(key) where key is a human-readable
    identifier for the process, such as 'Analyze image'. When the operation
    is completed, call lap_and_stop(key). Alternatively, if several
    identical operations are performed consecutively, you can call lap(key)
    and stop(key) when the last operation has completed. You can get
    statistics about the durations by calling get_time(key)
'''
import atexit
import numpy as np
from datetime import datetime
from functools import wraps


storage = {}


def start(key):
    if storage.get(key):
        storage[key]['start'] = datetime.now()
    else:
        storage[key] = {
            'start': datetime.now(),
            'times': []}
    return True


def lap_and_stop(key):
    lap(key)
    stop(key)
    return True


def stop(key):
    storage[key]['start'] = None
    return True


def lap(key):
    if key in storage and storage[key]['start'] is not None:
        storage[key]['times'].append((datetime.now() - storage[key]['start']))
        storage[key]['start'] = datetime.now()
        return True
    return False


def get_time(key):
    times = [
        t.microseconds + t.seconds * 1000000 for t in storage[key]['times']]
    data_dict = {
        'total': sum(times),
        'number': len(times),
        'average': np.average(times),
        'median': np.median(times),
        'deviation': np.std(times),
        'minimum': min(times) if len(times) else None,
        'maximum': max(times) if len(times) else None,
        'data': times
    }
    return data_dict


# Stopwatch decorator use: @time_it before function definition
def time_it(func):
    @wraps(func)
    def decorator(*args, **kwargs):
        start(func.__name__)
        retval = func(*args, **kwargs)
        lap_and_stop(func.__name__)
        return retval
    return decorator


def _unit_value(v):
    unit = 'microseconds'
    if v > 1000:
        v = round(v / 10) / 100.0
        unit = 'milliseconds'
        if v > 1000:
            v = round(v / 10) / 100.0
            unit = 'seconds'
            if v > 100:
                v = round(v / 6) / 10.0
                unit = 'minutes'

    return '%s %s' % (v, unit)


def get_time_string(key):
    data_dict = get_time(key)
    if data_dict['number'] == 0:
        return False
    string = (
        'Measurements for %s:\n'
        '------------------------------\n'
        'Number of runs: %s\n'
        'Total duration: %s\n'
        'Longest run: %s\n'
        'Shortest run: %s\n'
        'Average run: %s\n'
        'Median run: %s\n'
        'Standard deviation: %s\n') % (
            key, data_dict['number'], _unit_value(data_dict['total']),
            _unit_value(data_dict['maximum']),
            _unit_value(data_dict['minimum']),
            _unit_value(data_dict['average']),
            _unit_value(data_dict['median']),
            _unit_value(data_dict['deviation'])
        )
    return string


def print_time(key):
    print get_time_string(key)


def end_print():
    for key in sorted(
            storage.keys(),
            lambda a, b: get_time(b)['total'] - get_time(a)['total']):
        print_time(key)

atexit.register(end_print)
