from matplotlib import pyplot as plt
import numpy as np

steps = 350
delay = 0 # time_steps

p_t = 40
p_d = 30
p_rs = lambda t: max([rs  + (t - p_t) / (p_d / 40.0 + (t - p_t)) * (l_m - 1), 0.3 * ((t - p_t) ** 2.1 - ((t - p_t) * 0.35) ** 3)])

l_m = 3 # lunch multiplier to glucose release

rs = 8   # glucose release speed

ri = 1   # insulin production rate
ti = 0.09  # insulin half-life
mi = 0.05   # insulin production multiplier
mii = 0.095 # insulin impact multiplier

i0 = rs / mii  # start insulin
s0 = ti * i0 / mi  # start glucose

state = [[s0, i0, rs]]

for t in range(1, steps):
    if t >= p_t and t <= (p_d + p_t):
        rate = p_rs(t)
    elif t > p_d + p_t:
        rate = rs * (l_m * (t - p_d - p_t) / ((t - p_d - p_t) ** 1.15))
    else:
        rate = rs
    state.append([
        max([0, state[-1][0] + rate - mii * state[-1][1]]),
        max([0, state[-1][1] + state[-min([t, delay + 1])][0] * mi - ti * state[-1][1]]),
        rate])

print state
state = np.asarray(state)
plt.figure()
plt.plot(np.array(range(steps)) / 60.0, state[:, 0], c='red')
plt.plot(np.array(range(steps)) / 60.0, state[:, 1], c='blue')
plt.plot(np.array(range(steps)) / 60.0, state[:, 2], c='black')
plt.ylim([0, np.max([state[:, 0:2]])])
plt.xlim([0, steps / 60.0])
if delay <= 3:
    plt.title('Normal blood sugar regulation when having lunch')
else:
    plt.title('Blood sugar regulation with %s minute delay' % delay)
plt.legend(['Blood sugar level', 'Insulin level', 'Sugar intake'])
plt.ylabel('Concentration [a.u.]')
plt.xlabel('Time [hours]')
plt.savefig('simulation.pdf')
