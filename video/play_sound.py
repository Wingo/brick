import json
import numpy as np
from sounds import SoundManager
from time import sleep
from matplotlib import pyplot as plt


success_threshold = 40


def dst_to_wave(dst):
    max_dst = 1000
    min_dst = success_threshold
    longest_loop = 1
    shortest_loop = 0.2
    ratio = 1 / 3.0
    dst = min([dst, max_dst])
    dst = max([dst, min_dst])
    loop_freq = ((dst - min_dst) / float(max_dst - min_dst)) ** (0.5)
    loop_length = max([loop_freq * longest_loop, shortest_loop])
    frequency = 350 + (1 - loop_freq) * 600
    on = ratio * loop_length
    off = loop_length - on
    return on, off, frequency


sm = SoundManager()


f = open('new_session_98_testi.dat', 'r')
data = json.load(f)
f.close()
ons = []
offs = []
frequencies = []
for i, t in enumerate(data['exercises'][1]['times']):
    if i > 1:
        dist = np.sqrt(np.sum(((
            np.array(data['exercises'][1]['locations'][i]) -
            np.array(data['exercises'][1]['target'])) ** 2)))
        on, off, frequency = dst_to_wave(dist)
        ons.append(on)
        offs.append(off)
        frequencies.append(frequency)
#wave = sm.create_wave(1, 1, 500, 0.01)

plot_wave, wave = sm.play_sound_sequence(ons, offs, frequencies, 4/3.0 * np.array(data['exercises'][1]['times']))
sm.stream.write(wave)
plt.figure()
plt.plot(np.array(xrange(len(plot_wave))) / float(sm.bitrate), plot_wave)
plt.savefig('test.png')
'''
for i, t in enumerate(data['exercises'][1]['times']):
    if i > 1:
        dist = np.sqrt(np.sum(((
            np.array(data['exercises'][1]['locations'][i]) -
            np.array(data['exercises'][1]['target'])) ** 2)))
        on, off, frequency = dst_to_wave(dist)
        sm.set_mode(on, off, frequency)
        tm = (t - data['exercises'][1]['times'][i - 1]) / 1000000.0
        print tm
        sleep(float(tm))

'''
sm.stop()
