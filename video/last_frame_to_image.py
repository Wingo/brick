import cv2
import numpy as np
import json


def show_image(image, text='Image', time=0):
    resized_image = image.copy()
    max_dimensions = (750, 1200)
    dims = resized_image.shape
    if 0 in dims:
        print 'Cannot show picture without dimensions'
        return
    scale = min([
        float(max_dimensions[0]) / dims[0],
        float(max_dimensions[1]) / dims[1]])
    resized_image = cv2.resize(resized_image, None, fx=scale, fy=scale)
    cv2.imshow(text, resized_image)
    k = cv2.waitKey(time)
    cv2.destroyAllWindows()
    return k

if False:
    cap = cv2.VideoCapture('output.avi')
    frames = []
    i = 0
    while cap.isOpened():
        i += 1
        #cap.retrieve()
        print i
        ret, frame = cap.read()
        # show_image(frame)
        if i == 101:
            break


    cv2.imwrite('lastframe.jpg', frame)

frame = cv2.imread('lastframe.jpg')

f = open('new_session_5_yang.dat', 'r')
data = json.load(f)
f.close()

e = data['exercises'][3]

offset = [10, 100]
for i, l in enumerate(e['locations']):
    if i == 0:
        continue
    cv2.line(frame, tuple(e['locations'][i - 1]), tuple(l), (100, 0, 0), 3)

cv2.circle(
    frame,
    tuple(e['target']),
    30, (150, 0, 0), 3)
cv2.line(
    frame,
    (int(e['target'][0]) - 45, int(e['target'][1])),
    (int(e['target'][0]) + 45, int(e['target'][1])),
    (150, 0, 0), 3)
cv2.line(
    frame,
    (int(e['target'][0]), int(e['target'][1] - 45)),
    (int(e['target'][0]), int(e['target'][1] + 45)),
    (150, 0, 0), 3)

cv2.imwrite('out.jpg', frame)

f = open('new_session_71_hanyu.dat', 'r')
data = json.load(f)
f.close()

e = data['exercises'][0]

offset = [10, 100]
for i, l in enumerate(e['locations']):
    if i == 0:
        continue
    cv2.line(frame, tuple(e['locations'][i - 1]), tuple(l), (0, 0, 100), 3)

cv2.circle(
    frame,
    tuple(e['target']),
    30, (0, 0, 200), 3)
cv2.line(
    frame,
    (int(e['target'][0]) - 45, int(e['target'][1])),
    (int(e['target'][0]) + 45, int(e['target'][1])),
    (0, 0, 200), 3)
cv2.line(
    frame,
    (int(e['target'][0]), int(e['target'][1] - 45)),
    (int(e['target'][0]), int(e['target'][1] + 45)),
    (0, 0, 200), 3)

cv2.imwrite('out2.jpg', frame)
