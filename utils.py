import numpy as np
import cv2
from params import skipped_frames, VIDEO_RESOLUTION
from stopwatch import time_it, start, lap_and_stop


def initialize_camera(camera_index):
    camera = cv2.VideoCapture(camera_index)
    camera.set(
        cv2.CAP_PROP_FRAME_WIDTH, VIDEO_RESOLUTION[0])
    camera.set(
        cv2.CAP_PROP_FRAME_HEIGHT, VIDEO_RESOLUTION[1])
    return camera


@time_it
def capture_frame(camera):
    ret, frame = camera.read()
    return frame


def get_white_balance(img):
    sums = np.sum(img, axis=2)
    p = np.percentile(sums, 99)
    idxs = np.where(sums > p)
    pixels = img[idxs[0], idxs[1], :]
    wb_med = np.median(pixels, axis=0)
    wb = np.mean(wb_med) / wb_med
    return wb


@time_it
def apply_white_balance(img, wb):
    return img
    start('multiply')
    corrected = np.multiply(img, wb)
    lap_and_stop('multiply')
    start('minimization')
    corrected = np.clip(corrected, a_min=0, a_max=255)
    lap_and_stop('minimization')
    start('integerification')
    corrected = np.asarray(corrected, dtype=np.uint8)
    lap_and_stop('integerification')
    return corrected


def take_picture(camera_index):
    camera = initialize_camera(camera_index)
    for i in range(skipped_frames):
        pic = capture_frame(camera)
    stop_recording(camera)
    print 'Picture taken'
    return pic


@time_it
def resize_image(img, dims=None):
    max_dimensions = dims if dims is not None else (600, 800)
    dims = img.shape
    scale = min([
        float(max_dimensions[0]) / dims[0],
        float(max_dimensions[1]) / dims[1]])
    resized_image = cv2.resize(img, None, fx=scale, fy=scale)
    return resized_image


def show_image(image, text='Image', time=0):
    resized_image = image.copy()
    max_dimensions = (750, 1200)
    dims = resized_image.shape
    if 0 in dims:
        print 'Cannot show picture without dimensions'
        return
    scale = min([
        float(max_dimensions[0]) / dims[0],
        float(max_dimensions[1]) / dims[1]])
    resized_image = cv2.resize(resized_image, None, fx=scale, fy=scale)
    cv2.imshow(text, resized_image)
    k = cv2.waitKey(time)
    cv2.destroyAllWindows()
    return k


def stop_recording(camera):
    camera.release()
    cv2.destroyAllWindows()
