from oskui import get_files
import json
import cv2
import numpy as np
from matplotlib import pyplot as plt
from params import VIDEO_RESOLUTION

colors = ['0.1', '0.3', '0.6', '0.8', '0.8']


def strip_stationary(exercise):
    for j, l in enumerate(exercise['locations']):
        if np.sqrt((exercise['locations'][0][0] - l[0]) ** 2 + (exercise['locations'][0][1] - l[1]) ** 2) > 50:
            exercise['locations'] = exercise['locations'][j-1:]
            exercise['times'] = exercise['times'][j-1:]
            break
    return exercise

def score_dataset(dataset):
    for i, e in enumerate(dataset['exercises']):
        if 'finished' not in e or not e['finished']:
            print 'Not analyzed:', e
            continue
        e = strip_stationary(e)
        # exercise score
        e['score'], e['duration'], e['distance'], e['direct_path'] = get_stats(e)

    scores = [e.get('score') for e in dataset['exercises'] if 'score' in e.keys()]
    durations = [e.get('duration') for e in dataset['exercises'] if 'duration' in e.keys()]
    distances = [e.get('distance') for e in dataset['exercises'] if 'distance' in e.keys()]
    rel_durations = [e.get('duration') / float(e.get('direct_path')) for e in dataset['exercises'] if 'duration' in e.keys()]
    rel_distances = [e.get('distance') / float(e.get('direct_path')) for e in dataset['exercises'] if 'distance' in e.keys()]
    if len(scores) >= 1:
        mean_score = np.mean(scores)
        if mean_score > 0:
            mean_du = np.mean(durations)
            mean_di = np.mean(distances)
            return mean_score, mean_du, mean_di, scores, rel_distances, rel_durations
    return None

def analyze_data():
    plt.figure()
    plt.title('Distance and duration')
    scoredata = []
    outdata = []
    users = []
    for ind, fname in enumerate(get_files('data')):
        if '.dat' in fname:
            f = open('data/%s' % fname, 'r')
            data = json.load(f)
            f.close()

            stats = score_dataset(data)
            if stats is None:
                print 'Yielded None:'
                for i, e in enumerate(data['exercises']):
                    print e
                continue
            score, duration, distance, all_scores, rel_distances, rel_durations = stats
            delay = data['settings']['delay'] if data['settings']['delay'] is not None else 0
            for i in range(len(rel_distances)):
                dat = [
                    all_scores[i], rel_distances[i], rel_durations[i], delay,
                    data['settings']['username'], data['settings']['training'],
                    data['settings']['feedback_mode'], i, ind]
                users.append(dat)
            peaks = 0
            mode = settings_to_mode(data['settings'])
            scoredata.append([
                data['settings']['username'],
                score, duration, distance, peaks,
                mode, fname])
            if 'False-2-' in mode:
                plt.scatter(rel_durations, rel_distances, c=colors[delay])
                for p in range(len(rel_durations)):
                    outdata.append([score, rel_durations[p], rel_distances[p], delay])
    modes = list(set([s[-2] for s in scoredata]))
    for m in modes:
        print 'Scores for:', m
        for s in sorted(scoredata, key=lambda x: x[1]):
            if s[-2] == m:
                print s[1], s[0]

    plt.xlabel('Relative duration [seconds/pixel]')
    plt.ylabel('Relative distance [pixels]')
    plt.savefig('overview_scatter.jpg')
    f = open('results/outdata.json', 'w')
    json.dump(outdata, f)
    f.close()
    f = open('results/outusers.json', 'w')
    json.dump(users, f)
    f.close()
    return scoredata

def visualize_data():
    scoredata = analyze_data()
    if False:
        for entry in scoredata:
            f = open('data/%s' % entry[-1], 'r')
            data = json.load(f)
            f.close()
            mode = settings_to_mode(data['settings'])
            for i, e in enumerate(data['exercises']):
                if e['finished']:
                    score, duration, distance, direct_path = get_stats(exercise)
                    position = get_position(score, mode, scoredata)
                    draw(e, scoredata, data['settings'], 'results/%s_%s_%s_%s.jpg' % (
                        mode, position, entry[-1].split('.')[0], i))
    plot_overview(scoredata)


def get_stats(exercise):
    direct_path = np.sqrt(
        (exercise['locations'][0][0] - exercise['target'][0]) ** 2 +
        (exercise['locations'][0][1] - exercise['target'][1]) ** 2)

    duration = (exercise['times'][-1] - exercise['times'][0]) / 1000000.0
    distance = np.sum([np.sqrt(
        (exercise['locations'][i][0] - exercise['locations'][i - 1][0]) ** 2 +
        (exercise['locations'][i][1] - exercise['locations'][i - 1][1]) ** 2)
        for i in range(1, len(exercise['locations']))])
    score = (70 * duration + distance) / direct_path
    score = 1 / (1 + score) * 100
    # print 'Time penalty:', 70 * duration, 'Distance penalty', distance
    return score, duration, distance, direct_path

def get_position(score, mode, scoredata):
    scoredata.sort(key=lambda x: x[1], reverse=True)
    for i, entry in enumerate([s for s in scoredata if s[-2] == mode]):
        if entry[1] <= score:
            return i + 1

def settings_to_mode(settings):
    delay = settings['delay']
    delay = 0 if delay is None else delay
    mode = '%s-%s-%s' % (
        settings['training'],
        settings['feedback_mode'],
        delay)
    return mode

def draw(exercise, scoredata, settings, output):
    score = get_stats(exercise)
    position = get_position(score, settings_to_mode(settings), scoredata)
    plt.figure()
    plt.subplot(1, 2, 1)
    plt.plot([l[1] for l in exercise['locations']], [l[0] for l in exercise['locations']], c='black')
    plt.xlim([0, VIDEO_RESOLUTION[1]])
    plt.ylim([0, VIDEO_RESOLUTION[0]])
    plt.scatter(exercise['target'][1], exercise['target'][0], marker='x', c='red')
    plt.scatter(exercise['locations'][0][1], exercise['locations'][0][0], color='green', marker='o')
    plt.xlabel('X coordinate')
    plt.ylabel('Y coordinate')
    plt.gca().invert_xaxis()
    plt.gca().invert_yaxis()

    plt.subplot(1, 2, 2)
    plt.plot(
        (np.array(exercise['times']) - exercise['times'][0]) / 1000000.0,
        [np.sqrt(np.abs(l[0] - exercise['target'][0]) ** 2 + np.abs(l[1] - exercise['target'][1]) ** 2) for l in exercise['locations']],
        c='black')
    plt.xlabel('Time [seconds]')
    plt.ylabel('Distance [px]')
    plt.suptitle("%s's score: %s, position: %s" % (settings['username'], score, position))

    plt.subplots_adjust(left=0.125, bottom=0.1, right=0.9, top=0.9, wspace=0.4, hspace=0.2)

    plt.savefig(output)
    img = cv2.imread(output)
    return img

def plot_overview(scoredata):
    plt.figure()

    plt.title('Scored performance')
    plt.boxplot(
        [[s[1] for s in scoredata if s[-2] == 'False-2-%s' % i] for i in range(3)],
        0, positions=range(3), showfliers=False)
    plt.xlabel('Delay [seconds]')
    plt.ylabel('Score [points]')
    plt.savefig('overview_scores.jpg')

    plt.figure()
    plt.title('Exercise duration')
    plt.boxplot(
        [[s[2] for s in scoredata if s[-2] == 'False-2-%s' % i] for i in range(3)],
        0, positions=range(3), showfliers=False)
    plt.xlabel('Delay [seconds]')
    plt.ylabel('Average duration [seconds]')
    plt.savefig('overview_duration.jpg')

    plt.figure()
    plt.title('Distance travelled')
    plt.boxplot(
        [[s[3] for s in scoredata if s[-2] == 'False-2-%s' % i] for i in range(3)],
        0, positions=range(3), showfliers=False)
    plt.xlabel('Delay [seconds]')
    plt.ylabel('Average distance travelled [pixels]')
    plt.savefig('overview_distance.jpg')

    plt.figure()
    plt.title('Movement speed')
    plt.boxplot(
        [[s[3]/float(s[2]) for s in scoredata if s[-2] == 'False-2-%s' % i] for i in range(3)],
        0, positions=range(3), showfliers=False)
    plt.xlabel('Delay [seconds]')
    plt.ylabel('Speed [pixels/second]')
    plt.savefig('overview_speed.jpg')

visualize_data()