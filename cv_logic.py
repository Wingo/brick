import atexit
import cv2
import datetime
import json
import numpy as np
import os
import random
import stopwatch
import threading
import thread
from time import sleep

from oskui import get_files, toggle, choice_menu, lower_case_name,\
    press_any_key, prompt
from params import DEBUG, success_threshold, skipped_frames, VIDEO_RESOLUTION
from pre_gui import select_camera
from sounds import SoundManager
from utils import take_picture, initialize_camera, stop_recording,\
    capture_frame, get_white_balance, apply_white_balance, resize_image,\
    show_image

import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt


class Exercise(threading.Thread):
    """docstring for Exercise"""
    alive = True

    def __init__(
            self, camera_choice, camera,
            settings, objects, scheme,
            wb, home, gui, control_image):
        super(Exercise, self).__init__()
        self.camera = camera
        self.camera_choice = camera_choice
        self.scheme = scheme
        self.objects = objects
        self.settings = settings
        self.wb = wb
        self.home = home
        self.gui = gui
        self.control_image = control_image

        self.sm = SoundManager()
        atexit.register(self.sm.kill)
        self.sm.set_mode(0, 2, 400)
        self.sm.start()

        for o in objects:
            o['visible'] = False
            o['location'] = [0, 0]

    @stopwatch.time_it
    def get_locations(self):
        threshold = 50

        frame = capture_frame(self.camera)
        image = apply_white_balance(frame.copy(), self.wb)
        hsv_image = cv2.cvtColor(image.copy(), cv2.COLOR_BGR2HSV)

        diff = np.sqrt(np.sum(np.abs(
            np.asarray(image, dtype=np.float32) -
            np.asarray(self.control_image, dtype=np.float32)) ** 2, axis=2))

        diff_mask = np.zeros(diff.shape)
        diff_mask[np.where(diff > threshold)] = 255
        diff_mask = cv2.erode(diff_mask, None, iterations=2)
        diff_mask = cv2.dilate(diff_mask, None, iterations=2)

        for o in [o for o in self.objects if o['active']]:
            mask_hsv = cv2.inRange(
                hsv_image,
                np.array(o['filter'][0][0]),
                np.array(o['filter'][0][1]))
            mask_bgr = cv2.inRange(
                image,
                np.array(o['filter'][1][0]),
                np.array(o['filter'][1][1]))
            zz = np.zeros(frame.shape, dtype=np.uint8)
            mask = cv2.bitwise_and(mask_bgr, mask_hsv)
            mask = cv2.bitwise_and(
                np.asarray(mask, dtype=np.uint8),
                np.asarray(diff_mask, dtype=np.uint8))
            mask = cv2.erode(mask, None, iterations=2)
            mask = cv2.dilate(mask, None, iterations=2)
            im2, contours, h = cv2.findContours(
                mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            if len(contours) > 0:
                o['visible'] = True
                c = max(contours, key=lambda x: cv2.contourArea(x))
                c = cv2.convexHull(c)
                M = cv2.moments(c)
                cx = int(M['m10'] / M['m00'])
                cy = int(M['m01'] / M['m00'])
                o['location'] = [cx, cy]
                image[cy, cx, :] = 255
                if DEBUG:
                    cv2.putText(
                        image, o['name'] + str(o['location']),
                        (cx, cy),
                        cv2.FONT_HERSHEY_SIMPLEX, 1.0,
                        (0, 0, 0), 3)
                    cv2.drawContours(zz, [c], 0, (0, 0, 255), -1)
                    zz[:, :, 0] = mask_hsv
                    zz[:, :, 1] = mask_bgr
            else:
                o['visible'] = False
        visibles = [o for o in self.objects if o['visible']]
        if False:
            return visibles, zz#np.asarray(np.stack([diff for s in range(3)], -1), dtype=np.uint8)
        else:
            return visibles, image

    @stopwatch.time_it
    def run(self):
        start = datetime.datetime.now()
        session_data = {
            'settings': self.settings,
            'exercises': []
        }
        take_video = False

        for ex_index, target_dst in enumerate(self.scheme):
            ex_data = {
                'target_distance': target_dst,
                'target': None,
                'home': list(self.home),
                'locations': [],
                'times': [],
                'exercise_index': ex_index,
                'finished': False
            }
            if not self.alive:
                break
            target = None  # x, y
            for i in range(10):
                angle = random.random() * 2 * np.pi
                target = np.array(self.home) + np.array(
                    [np.cos(angle), np.sin(angle)]) * target_dst * 100
                target = np.asarray(target, dtype=int)
                if (target[0] > 50 and target[1] > 50 and (
                        target[0] < VIDEO_RESOLUTION[0] - 50 and (
                            target[1] < VIDEO_RESOLUTION[1] - 50))):
                    ex_data['target'] = list(target)
                    break
            if target is None:
                raise TypeError('Target not specified')
            # cv2.namedWindow("live")
            # if not self.settings['auto_tick']:
            #    cv2.setMouseCallback("live", self.mouse_pos)

            # Bring to home position
            if ex_index != 0:
                self.sm.set_double(400, 600, sleep_amount=2)
            else:
                self.sm.set_double(400, 600)
            while self.alive:
                visibles, image = self.get_locations()

                cv2.circle(
                    image,
                    (int(self.home[0]), int(self.home[1])),
                    30, (0, 0, 255), 3)
                cv2.line(
                    image,
                    (int(self.home[0]) - 45, int(self.home[1])),
                    (int(self.home[0]) + 45, int(self.home[1])),
                    (0, 0, 255), 3)
                cv2.line(
                    image,
                    (int(self.home[0]), int(self.home[1] - 45)),
                    (int(self.home[0]), int(self.home[1] + 45)),
                    (0, 0, 255), 3)
                image = np.rot90(image, k=3, axes=(0, 1))
                thread.start_new_thread(
                    self.gui.update_video,
                    (cv2.cvtColor(resize_image(image), cv2.COLOR_BGR2RGB),))

                if len(visibles) > 0:
                    current = [o for o in self.objects if o['visible']][0]
                    dst = np.sqrt(
                        (current['location'][0] - self.home[0]) ** 2 + (
                            current['location'][1] - self.home[1]) ** 2)  # x,y
                    if dst < success_threshold * 2:
                        break



            # Define the codec and create VideoWriter object
            fourcc = cv2.VideoWriter_fourcc(*'MJPG')
            if take_video:
                out = cv2.VideoWriter('output.avi',fourcc, 5.0, image.shape[:2])

            point_set = []
            self.sm.set_mode(0, 2, 0)
            while self.alive:
                stopwatch.start('Exercise loop')
                visibles, image = self.get_locations()
                if len(visibles) > 0:
                    current = [o for o in self.objects if o['visible']][0]
                    dst = np.sqrt((current['location'][0] - target[0]) ** 2 + (
                        current['location'][1] - target[1]) ** 2)
                    point_set.append(current['location'])
                    if len(point_set) > 1:
                        for i, p in enumerate(point_set):
                            if i == 0:
                                continue
                            cv2.line(
                                image,
                                tuple(point_set[i - 1]),
                                tuple(p),
                                (0, 150, 0), 3)
                    t = datetime.datetime.now() - start
                    t_point = t.microseconds + t.seconds * 10 ** 6
                    ex_data['locations'].append(list(current['location']))
                    ex_data['times'].append(t_point)
                    if dst < success_threshold:
                        self.sm.set_mode(1, 0.1, 800, win=True)
                        ex_data['finished'] = True
                        break
                    if self.settings['auto_tick']:
                        self.dst_to_wave(dst)

                # Draw target
                cv2.circle(
                    image,
                    (int(target[0]), int(target[1])),
                    success_threshold, (0, 255, 0), 3)
                cv2.line(
                    image,
                    (int(target[0]) - (success_threshold + 15), int(target[1])),
                    (int(target[0]) + (success_threshold + 15), int(target[1])),
                    (0, 255, 0), 3)
                cv2.line(
                    image,
                    (int(target[0]), int(target[1] - (success_threshold + 15))),
                    (int(target[0]), int(target[1] + (success_threshold + 15))),
                    (0, 255, 0), 3)
                image = np.rot90(image, k=3, axes=(0, 1))

                if not take_video:
                    thread.start_new_thread(
                        self.gui.update_video,
                        (cv2.cvtColor(resize_image(image), cv2.COLOR_BGR2RGB),))
                else:
                    out.write(np.rot90(image, k=1, axes=(0, 1)))

                stopwatch.lap_and_stop('Exercise loop')
            if take_video:
                out.release()
            session_data['exercises'].append(ex_data)

        session_data['timestamp'] = datetime.datetime.now().strftime(
            "%Y-%m-%d %I:%M:%S %p")

        fs = get_files('data')
        numbers = [int(f.split('_')[-1].split('.')[0]) if 'tracker' in f else int(f.split('_')[-2]) for f in fs if '.dat' in f]
        numbers.sort()
        if len(numbers) == 0:
            n = 0
        else:
            n = numbers[-1] + 1

        f = open('data/new_session_%s_%s.dat' % (
            n, session_data['settings']['username']), 'w')
        json.dump(session_data, f)
        f.close()



        self.gui.enable(True)
        self.sm.alive = False

    def dst_to_wave(self, dst):
        max_dst = 1000
        min_dst = success_threshold
        longest_loop = 1
        shortest_loop = 0.2
        ratio = 1 / 3.0
        dst = min([dst, max_dst])
        dst = max([dst, min_dst])
        if self.settings['auto_tick']:
            loop_freq = ((dst - min_dst) / float(max_dst - min_dst)) ** (0.5)
        else:
            loop_freq = (dst - min_dst) / float(max_dst - min_dst)
        loop_length = max([loop_freq * longest_loop, shortest_loop])
        frequency = 350 + (1 - loop_freq) * 600
        on = ratio * loop_length
        off = loop_length - on
        self.sm.set_mode(on, off, frequency, self.settings['delay'])


def initialize(interaction=False):
    number = 0
    while True:
        try:
            camera = initialize_camera(number)
            if camera.isOpened():
                number += 1
                stop_recording(camera)
            else:
                break
        except:
            print 'Error encountered'
            break
    print 'There are %s cameras plugged in.' % (number)

    if number >= 2:
        photos = [take_picture(ind) for ind in range(number)]
        camera_choice = select_camera(photos)
        print "Got:", camera_choice
    elif number == 1:
        camera_choice = 0
    else:
        camera_choice = False

    if camera_choice is not False:

        control_image = take_picture(camera_choice)
        wb = get_white_balance(control_image)

        control_image = apply_white_balance(control_image, wb)
        control_image_float = np.asarray(
            cv2.cvtColor(control_image, cv2.COLOR_BGR2HSV), np.float32)
        if not os.path.exists('objects'):
            os.makedirs('objects')

        camera = initialize_camera(camera_choice)
        for i in range(skipped_frames):
            capture_frame(camera)
    else:
        return

    # Find circle center
    gray_control = cv2.cvtColor(control_image, cv2.COLOR_BGR2GRAY)
    circles = cv2.HoughCircles(
        gray_control, cv2.HOUGH_GRADIENT,
        dp=2.3, minDist=1, param1=300,
        param2=150, minRadius=50, maxRadius=500)
    if circles is not None:
        shimage = gray_control.copy()
        c = sorted(circles[0], key=lambda x: x[2])[0]
        cv2.circle(shimage, (c[0], c[1]), c[2], (255), 5)
        home = [float(c[0]), float(c[1])]
    else:
        print 'No circles'
        home = np.array(control_image.shape) / 2
        home = home[:2]

    settings = {
        'auto_tick': True,
        'training': True,
        'delay': 0
    }

    objects = []
    fs = [f for f in get_files('objects') if '.json' in f]
    if len(fs):
        if interaction: # Let user train and load objects
            choice = None
            while choice is not False:
                choice = choice_menu([
                    'Load objects',
                    'Train new object',
                    'Select active objects',
                    'Edit settings'], 'Select an option:')
                if choice is False:
                    break
                elif choice == 0:
                    lst = toggle(
                        [f.split('.')[0] for f in fs],
                        [0 for f in fs],
                        'Select previous objects for import:')
                    for ind in range(len(lst)):
                        if lst[ind] == 1:
                            f = open('objects/%s' % fs[ind], 'r')
                            data = json.load(f)
                            f.close()
                            object_filters = [[np.asarray(o) for o in flt] for flt in data]
                            objects.append({
                                'name': fs[ind].split('.')[0],
                                'filter': object_filters, 'active': False})
                elif choice == 1:
                    name = lower_case_name('Give name for the object:')
                    object_filter = train_object(
                        name, control_image_float, camera_choice)
                    objects.append(
                        {'name': name, 'filter': object_filter, 'active': True})
                elif choice == 2:
                    choice = choice_menu(
                        [o['name'] for o in objects],
                        [int(o['active']) for o in objects],
                        'Select active objects:')
                    for i, o in enumerate(objects):
                        if i == choice:
                            objects[choice]['active'] = True
                        else:
                            [choice]['active'] = False
                elif choice == 3:
                    out = toggle(
                        settings.keys(),
                        [1 if settings[s] else 0 for s in settings],
                        'Toggle settings, green is on:')
                    for i, k in enumerate(settings.keys()):
                        settings[k] = out[i]
        else: # load all objects and continue
            for fname in fs:
                f = open('objects/%s' % fname, 'r')
                data = json.load(f)
                f.close()
                object_filters = [[np.asarray(o) for o in flt] for flt in data]
                objects.append({
                    'name': fname.split('.')[0],
                    'filter': object_filters, 'active': False})

    else:
        train_new = True
        while train_new:
            name = lower_case_name('Give name for the object:\n')
            object_filters = train_object(name, control_image_float, camera_choice)
            objects.append(
                {'name': name, 'filter': list(object_filters), 'active': True})
            train_new = prompt('Train another object?:', True)

    return settings, objects, camera_choice, wb, camera, home, control_image


def train_object(name, control_image_float, camera_choice):
    spread = 1.5

    threshold_h = 10
    threshold_hi = 50
    threshold_s = 10
    threshold_si = 50
    threshold_v = 0
    threshold_vi = 50
    add_one = True
    high = np.array([255, 255, 255], dtype=np.uint8)
    areas = []
    size_index = 0
    colors_hsv = [[], [], []]
    colors_bgr = [[], [], []]
    while add_one:
        f = plt.figure()
        press_any_key((
            'Train new object to track by placing it on the'
            ' workspace and press any key to take picture.'))
        frame = take_picture(camera_choice)
        hsv_frame_float = np.asarray(
            cv2.cvtColor(frame.copy(), cv2.COLOR_BGR2HSV), np.float32)

        max_dimensions = (750, 1200)
        dims = frame.shape
        scale = min([
            float(max_dimensions[0]) / dims[0],
            float(max_dimensions[1]) / dims[1]])

        cv2.namedWindow("image")
        cv2.createTrackbar('H', 'image', threshold_h, 100, lambda x: None)
        cv2.createTrackbar('S', 'image', threshold_s, 100, lambda x: None)
        cv2.createTrackbar('V', 'image', threshold_v, 100, lambda x: None)
        cv2.createTrackbar('HI', 'image', threshold_hi, 100, lambda x: None)
        cv2.createTrackbar('SI', 'image', threshold_si, 100, lambda x: None)
        cv2.createTrackbar('VI', 'image', threshold_vi, 100, lambda x: None)
        while True:
            threshold_h = cv2.getTrackbarPos('H', 'image')
            threshold_s = cv2.getTrackbarPos('S', 'image')
            threshold_v = cv2.getTrackbarPos('V', 'image')
            threshold_hi = cv2.getTrackbarPos('HI', 'image')
            threshold_si = cv2.getTrackbarPos('SI', 'image')
            threshold_vi = cv2.getTrackbarPos('VI', 'image')
            low = np.asarray([
                threshold_h * 2.55,
                threshold_s * 2.55,
                threshold_v * 2.55], np.uint8)
            high = np.asarray([
                threshold_hi * 2.55,
                threshold_si * 2.55,
                threshold_vi * 2.55], np.uint8)
            mask = cv2.inRange(frame, low, high)
            mask = cv2.erode(mask, None, iterations=2)
            mask = cv2.dilate(mask, None, iterations=2)

            pre_img = cv2.bitwise_and(frame, frame, mask=mask).copy()

            im2, contours, h = cv2.findContours(
                mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            if len(contours) > 0:
                size_index = np.min([np.max([size_index, 0]), len(contours)])
                contours.sort(key=lambda x: -cv2.contourArea(x))
                c = contours[size_index]
                hull = cv2.convexHull(c)
                cv2.drawContours(pre_img, [hull], 0, (255, 1, 1), thickness=3)

            resized_image = cv2.resize(pre_img, None, fx=scale, fy=scale)
            cv2.imshow('image', resized_image)
            key = cv2.waitKey(1)
            if key in [27, 13]:
                sleep(0.5)
                cv2.destroyAllWindows()
                sleep(0.5)
                cv2.destroyAllWindows()
                break
            elif key == 119:
                size_index = np.min([size_index + 1, len(contours), 5])
            elif key == 115:
                size_index = np.max([size_index - 1, 0])

        hull_mask = np.zeros(mask.shape)
        cv2.drawContours(hull_mask, [hull], 0, (255, 255, 255), thickness=-1)
        mask_indices = np.nonzero(hull_mask)
        for k in range(len(mask_indices[0])):
            for i in range(3):
                colors_hsv[i].append(
                    hsv_frame_float[mask_indices[0][k], mask_indices[1][k], i])
                colors_bgr[i].append(
                    frame[mask_indices[0][k], mask_indices[1][k], i])
        areas.append(cv2.contourArea(hull))
        add_one = prompt(
            'Would you like to add an image to increase accuracy?')
    plt.figure()
    plt.subplot(3, 2, 1)
    plt.hist(colors_hsv[0], bins=50, color='red')
    plt.xlim([0, 255])
    plt.title('H')
    plt.subplot(3, 2, 2)
    plt.hist(colors_hsv[1], bins=50, color='red')
    plt.xlim([0, 255])
    plt.title('S')
    plt.subplot(3, 2, 3)
    plt.hist(colors_hsv[2], bins=50, color='red')
    plt.xlim([0, 255])
    plt.title('V')
    plt.subplot(3, 2, 4)
    plt.hist(colors_bgr[0], bins=50, color='red')
    plt.xlim([0, 255])
    plt.title('B')
    plt.subplot(3, 2, 5)
    plt.hist(colors_bgr[1], bins=50, color='red')
    plt.xlim([0, 255])
    plt.title('G')
    plt.subplot(3, 2, 6)
    plt.hist(colors_bgr[2], bins=50, color='red')
    plt.xlim([0, 255])
    plt.title('R')
    plt.savefig('%s_filter.jpg' % name)
    object_filter_hsv = [
        [np.mean(colors_hsv[i]) - spread * np.std(colors_hsv[i]) for i in range(3)],
        [np.mean(colors_hsv[i]) + spread * np.std(colors_hsv[i]) for i in range(3)],
        np.mean(areas)]
    object_filter_bgr = [
        [np.mean(colors_bgr[i]) - spread * np.std(colors_bgr[i]) for i in range(3)],
        [np.mean(colors_bgr[i]) + spread * np.std(colors_bgr[i]) for i in range(3)],
        np.mean(areas)]
    '''
    for i in range(3):
        plt.subplot(3, 1, i + 1)
        plt.hist(colors[i], 30)
        axes = plt.gca()
        axes.set_xlim([0, 255])
    plt.savefig('plot_%s.pdf' % len(name))
    '''
    f = open('objects/%s.json' % name, 'w')
    json.dump([object_filter_hsv, object_filter_bgr], f)
    f.close()

    contour_frame = frame.copy()
    cv2.drawContours(contour_frame, [hull], 0, (255, 255, 255), thickness=-1)
    cv2.imwrite('%s_contour.jpg' % name, contour_frame)

    bgr_frame = frame.copy()
    mask = cv2.inRange(bgr_frame, np.asarray(object_filter_bgr[0]), np.asarray(object_filter_bgr[1]))
    bgr_frame = cv2.bitwise_and(bgr_frame, bgr_frame, mask=mask)
    cv2.imwrite('%s_bgr.jpg' % name, bgr_frame)

    hsv_frame = np.asarray(hsv_frame_float.copy(), dtype=np.uint8)
    mask = cv2.inRange(hsv_frame, np.asarray(object_filter_hsv[0]), np.asarray(object_filter_hsv[1]))
    hsv_frame = cv2.bitwise_and(hsv_frame, hsv_frame, mask=mask)
    cv2.imwrite('%s_hsv.jpg' % name, hsv_frame)

    return object_filter_hsv, object_filter_bgr
